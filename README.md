# Bomber Coso

This is a simple 2D game developed in Java Applet.

Just a small projet to learn threading and mouse events in Java.

Bombs will spam randomly on the screen. Faster with time.
You need to click on them to pause them before they explode.
But if you click them again. The y will restart to consume.

You can group them together to free up the space.

![ezgif-2-79fd62cf59.gif](images/ezgif-2-79fd62cf59.gif)

## Requirements

- Java (6 to) 8 (max, recommended)
- NetBeans (highly recommended)

Java Applets have been dropped with Java 9. So you need an older versione to run this project.
Java 8 is the preferred version. Until Java 6 should be ok.

Problems running the project are expected, applets are deprecated and not supported by modern browsers.**

This is a NetBeans Project. Using NetBeans isn't mandatory but recommended for the built-in applet runner.
You can run the GrigliaApplet.java class with netbeans.

[NetBeans Instructions](https://netbeans.apache.org/tutorial/main/kb/docs/java/javase-jdk8/)

Alternatively, you can use IntellijJ IDEA with the [Applet runner plugin](https://plugins.jetbrains.com/plugin/16682-applet-runner) (not tested).

## Installation and run

Clone the repo:

```bash
git clone https://gitlab.com/stefanorossiti/bomber-coso
```

Run `AppletBomberCoso` with your preferred IDE (NetBeans is recommended) using 
java 8.

## License

All my work is released under [DBAD](https://www.dbad-license.org/) license.
