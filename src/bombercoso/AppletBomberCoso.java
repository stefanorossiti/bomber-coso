/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bombercoso;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
/**
 *
 * @author BBC
 */
public class AppletBomberCoso extends Applet implements MouseListener, MouseMotionListener, Runnable, BomberListener{
    //DOUBLE BUFFERING
     Graphics bufferGraphics;
     Image offscreen;
    
    //array di bombe, inserire il num max di bombe
    Bomba[] b = new Bomba[500];
    private int bombCnt = 0;
    //inserire ogni quanto spawnare una nuova bomba in ms
    private int msToNextBomb = 2000;
    private int msAggContext = 20;
    //istanzio lo scorecontroller
    private ScoreController sc = new ScoreController();
    
    Thread thAggContext = new Thread(this);
    
    @Override
    public void init(){
        this.setSize(600,600);
        //System.out.println("Start");
        
        thAggContext.start();
    }
    
     @Override
     public void update(Graphics g) 
     { 
          paint(g);
     }
    
    @Override
    public void paint(Graphics g){
        //gewstione offscreen
        offscreen = createImage(this.getWidth() ,this.getHeight()); 
        bufferGraphics = offscreen.getGraphics();
        //cancello il contenuto
        bufferGraphics.clearRect(0,0,this.getWidth() ,this.getHeight());
        
        //disegno la bomba con eventualmente il tempo rimanente prima di scoppiare,
        //se la bomba è gia scoppiata disegno un cerchio rosso pieno
        
        for(int i = 0; i < bombCnt && i < b.length; i++){
            if(b[i].getTimeToBoom() <= 0.01){
                //disegno il contorno della bomba
                bufferGraphics.setColor(Color.BLACK);
                bufferGraphics.drawOval((int)b[i].getUpperLeftCorner().getX(), (int)b[i].getUpperLeftCorner().getY(), b[i].getRaggio()*2, b[i].getRaggio()*2);

                //cerchio rosso
                bufferGraphics.setColor(Color.BLACK);
                bufferGraphics.fillOval((int)b[i].getUpperLeftCorner().getX(), (int)b[i].getUpperLeftCorner().getY(), b[i].getRaggio()*2, b[i].getRaggio()*2);
            }else{
                //disegno il background delal bomba
                bufferGraphics.setColor(Color.WHITE);
                bufferGraphics.fillOval((int)b[i].getUpperLeftCorner().getX(), (int)b[i].getUpperLeftCorner().getY(), b[i].getRaggio()*2, b[i].getRaggio()*2);

                //disegno il riempimento della bomba che indica il tempo allo scoppio
                //a dipendenza di quanto tempo rimane il colore del riempimento cambia
                if(b[i].getTimeToBoom() <= 3){
                    bufferGraphics.setColor(Color.RED);
                }else if(b[i].getTimeToBoom() <= 7){
                    bufferGraphics.setColor(Color.ORANGE);
                }
                else{
                    bufferGraphics.setColor(Color.GREEN);
                }
                bufferGraphics.fillArc((int)b[i].getUpperLeftCorner().getX(), (int)b[i].getUpperLeftCorner().getY(), b[i].getRaggio()*2, b[i].getRaggio()*2, -90, (int)((360/b[i].getBegginningTimeToBoom())* b[i].getTimeToBoom()));

                //disegno la miccia della bomba
                bufferGraphics.drawArc((int)b[i].getUpperLeftCorner().getX() + b[i].getRaggio(), (int)b[i].getUpperLeftCorner().getY() - b[i].getRaggio(), b[i].getRaggio()*2, b[i].getRaggio()*2, 180, (int)((-1)*(180/b[i].getBegginningTimeToBoom())* b[i].getTimeToBoom()));
                
                //disegno il contorno della bomba
                bufferGraphics.setColor(Color.BLACK);
                bufferGraphics.drawOval((int)b[i].getUpperLeftCorner().getX(), (int)b[i].getUpperLeftCorner().getY(), b[i].getRaggio()*2, b[i].getRaggio()*2);

                //scrivo il timer della bomba, se è minore uguale a 3 lo scrive in rosso
                if(b[i].getTimeToBoom() <= 3){
                    bufferGraphics.setColor(Color.RED);
                }else{
                    bufferGraphics.setColor(Color.BLACK);
                }
                
                //scrivo il numero di conteggio allo scoppio
                //bufferGraphics.drawString(String.valueOf(b[i].getTimeToBoom()),(int)b[i].getCentro().getX()-3, (int)b[i].getCentro().getY() + 3);
            }
        }
        //stampo il punteggio del gioco
        bufferGraphics.setColor(Color.LIGHT_GRAY);
        bufferGraphics.fillRect(0, 0, 100, 20);
        bufferGraphics.setColor(Color.BLACK);
        bufferGraphics.drawString("Punteggio: " + sc.getScore(), 10, 15);
        
        g.drawImage(offscreen,0,0,this); 
    }
    
    @Override
    public void run(){
        int i = msToNextBomb/msAggContext-1;
        while(thAggContext.isAlive()){
            //ogni secondo (40 volte il ciclo con pausa di 25 ms) viene istanziana una nuova bomba
            i++;
            if(i == msToNextBomb/msAggContext){
                //System.out.println("nuovabomber");
                //istanzio una nuova bomba alla posizione dell array di bombe iche conta quante bombe ci sono gia e aggiungo i listener
                if (bombCnt < b.length){
                    b[bombCnt] = new Bomba(this);
                    this.addMouseListener(b[bombCnt]);
                    this.addMouseMotionListener(b[bombCnt]);
                }
                
                bombCnt++;
                i=0;
            }
            
            try {
                //repaint ogni 25 ms
                repaint();
                Thread.sleep(msAggContext); //l'occhio vede a max 30hz --> 1000/30 = 33.3
                //System.out.println("agg");
                
            }catch(InterruptedException e){
                System.out.println("InterruptedExceptionContextAgg");
            }
        }
    }
    
    /////////////////////////
    // BOMBER INTERFACE
    /////////////////////////
    @Override
    public void boom(int ID){
        System.out.println("bomba " + ID +  " esplosa");
        //thAggContext.stop();
        repaint();
    }
    
    @Override
    public void disabled(int ID) {
        System.out.println("bomba " + ID +  " disabilitata");
    }

    @Override
    public void rehabilitated(int ID) {
        System.out.println("bomba " + ID +  " riabilitata");
    }
    
    /////////////////////////
    // MOUSE LISTENER
    /////////////////////////
    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseDragged(MouseEvent e) {
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }
}