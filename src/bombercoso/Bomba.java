/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bombercoso;

import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 *
 * @author BBC
 */
//la bomba è un oggetto circolare dotata di timer interno che parte o si 
//ferma in base ai click dell utente
public class Bomba implements Runnable, MouseListener, MouseMotionListener{    
    //grandezza del contesto grafico
    private static final int wGC = 600, hGC = 600;
    private int ID; //id della bomba
    private boolean activated;
    private static int countID = 0; //variabile per il conteggio delle bombe per l assegnazione dell ID
    private Point centro; //punto del centro della bomba
    private int raggio; //raggio della bomba
    private float timeToBoom;
    private final float beginningTimeToBoom;
    private Point upperLeftCorner; // angolo alto sinistro del quadrato in cui e inscritto il cerchio della bomba
    private Thread timer = new Thread(this);
    
    private BomberListener bInt;
    
    //costrittore di default, quqando istanzio un oggetto bomba dovro passare 
    //come paramentro chi riceve i "segnali" di BomberListener 
    //poi devo far combaciare i 2 oggetti tramite il costruttore
    public Bomba(BomberListener bInt){
        //faccio combaciare la classe istanziante con l oggetto istanziato
        this.bInt = bInt;
        
        this.ID = Bomba.countID;
        Bomba.countID++;
        
        //genero misure della bomba        
        randomBombMeasures();
        this.beginningTimeToBoom = this.timeToBoom;
        
        //calcolo upperLeftCorner
        upperLeftCornerCompute();
        
        //inizio il decremento di timeToBoom tramite il thread timer
        this.activated = true;
        beginCountdown();
    }
    
//    public Bomba(int ID, Point centro, int raggio, int timeToBoom){
//        this.ID = this.countID;
//        this.countID++;
//    }
    
    private void upperLeftCornerCompute(){
        this.upperLeftCorner = new Point((int)centro.getX() - raggio, (int)centro.getY() - raggio);
    }
    
    private void randomBombMeasures(){
        //genero una misura casuale del raggio che da da 20 a 40 px
        this.raggio = (int)((Math.random()*100)/5)+20;
        
        //genero una misura casuale del centro, controlla che la bomba non esce dai bordi (0,0) e (wGC,hGC)
        do{
            this.centro = new Point((int)(Math.random()*wGC)-this.raggio, (int)(Math.random()*hGC)-this.raggio);
        }while(centro.getX()-raggio < 0 || centro.getY()-raggio < 0);
        
        //genero un timing casuale per la bomba da 5 a 15 che poi saranno secondi
        this.timeToBoom = (int)(Math.random()*10 + 5);
    }
    
    private void beginCountdown(){
        if(this.activated && this.timeToBoom > 0){
            this.timer.start();
            this.activated = true;
        }
    }
    private void resumeCountdown(){
        if(!this.activated && this.timeToBoom > 0){
            this.timer.resume();
            this.activated = true;
            this.bInt.rehabilitated(this.ID);
            
            //System.out.println("Bomba: " + this.ID + " Reactivated");
        }
    }
    private void pauseCountdown(){
        if(this.activated){
            this.timer.suspend();
            this.activated = false;
            this.bInt.disabled(this.ID);
            //System.out.println("Bomba: " + this.ID + " Neutralized");
        }
    }
    private void stopCountdown(){
        if(this.timer.isAlive()){
            this.timer.stop();
            this.activated = false;
        }
    }
    
    @Override
    public void run(){
        while(this.timer.isAlive()){
            try {
                //System.out.println("Bomba: " + this.ID + " Explods in: " + this.timeToBoom);
                Thread.sleep(100);
                this.timeToBoom = (float)(this.timeToBoom - 0.1);
            }catch (InterruptedException e){
                System.out.println("InterruptedExceptionBomb" + this.ID);
            }
            
            if(this.timeToBoom <= 0.01){
                //System.out.println("Bomba: " + this.ID + " BOOOM");
                bInt.boom(this.ID);
                stopCountdown();
            }
        }
    }
    
    /////////////////////////
    // SETTER E GETTER 
    /////////////////////////
    public Point getUpperLeftCorner(){
        return this.upperLeftCorner;
    }
    
    public int getRaggio(){
        return this.raggio;
    }

    public Point getCentro(){
        return this.centro;
    }
    
    public Float getTimeToBoom(){
        return this.timeToBoom;
    }
    
    public float getBegginningTimeToBoom(){
        return this.beginningTimeToBoom;
    }
    
    /////////////////////////
    // MOUSE LISTENER
    /////////////////////////
    @Override
    public void mouseClicked(MouseEvent e) {
        //controllo se ho cliccato nella area del duadrato in cui e circoncritta la bomba
        if(e.getX() > upperLeftCorner.getX() && e.getY() > upperLeftCorner.getY() && e.getX() < upperLeftCorner.getX() + 2*raggio && e.getY() < upperLeftCorner.getY() + 2*raggio){
            if(this.activated){
                pauseCountdown();
            }else{
                resumeCountdown();
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }
    @Override
    public void mouseReleased(MouseEvent e) {  
    }
    @Override
    public void mouseEntered(MouseEvent e) {     
    }
    @Override
    public void mouseExited(MouseEvent e) {    
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        //con il talso sx le bombe vengono prese
        if(e.getX() > upperLeftCorner.getX() && e.getY() > upperLeftCorner.getY() && e.getX() < upperLeftCorner.getX() + 2*raggio && e.getY() < upperLeftCorner.getY() + 2*raggio){ 
            //System.out.println("mov");
            if (e.getModifiers() == MouseEvent.BUTTON1_MASK) {
                Point click = new Point(e.getX(), e.getY());
                
                centro.setLocation(click);
                
                upperLeftCornerCompute();
            }
        }
        
        //con il talso dx le bombe vengono spinte
        if(e.getX() > upperLeftCorner.getX() && e.getY() > upperLeftCorner.getY() && e.getX() < upperLeftCorner.getX() + 2*raggio && e.getY() < upperLeftCorner.getY() + 2*raggio){ 
            //System.out.println("mov");
            if (e.getModifiers() == MouseEvent.BUTTON3_MASK) {
                Point click = new Point(e.getX(), e.getY());
                
                centro.setLocation(centro.getX() - (click.getX() - centro.getX()), centro.getY() - (click.getY() - centro.getY()));
                
                upperLeftCornerCompute();
            }
        }
    }

    @Override
    public void mouseMoved(MouseEvent e){
        
    }
}