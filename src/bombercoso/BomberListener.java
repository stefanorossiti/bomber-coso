/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bombercoso;

/**
 *
 * @author BBC
 */
public interface BomberListener {
    //si attiva quando viene cliccata la bomba e restituisce le la bomba è attiva o no
    public void boom(int ID);
    
    public void disabled(int ID);
    
    public void rehabilitated(int ID);
}
